﻿namespace carritoCompra;

/// <summary>
/// Representa un conjunto de promociones basadas en la cantidad de productos en el carrito.
/// </summary>
public class PromocionCantidad
{

    /// <summary>
    /// Obtiene una lista de promociones basadas en la cantidad de productos.
    /// </summary>
    /// <returns>Una lista de promociones por cantidad de productos.</returns>
    public static List<Promocion> ObtenerPromocionesCantidad()
    {
        List<Promocion> promociones = new List<Promocion>();

        Promocion promocion3Productos = new Promocion
        {
            Descripcion = "Descuento por cantidad - 3 productos",
            PorcentajeDescuento = 0.05m,
            Categoria = new Categoria { Nombre = "Todos" },
            CantidadMinimaProductos = 3
        };
        promociones.Add(promocion3Productos);

        Promocion promocion5Productos = new Promocion
        {
            Descripcion = "Descuento por cantidad - 5 productos",
            PorcentajeDescuento = 0.10m, 
            Categoria = new Categoria { Nombre = "Todos" },
            CantidadMinimaProductos = 5
        };
        promociones.Add(promocion5Productos);

        Promocion promocion10Productos = new Promocion
        {
            Descripcion = "Descuento por cantidad - 10 productos",
            PorcentajeDescuento = 0.15m, 
            Categoria = new Categoria { Nombre = "Todos" },
            CantidadMinimaProductos = 10
        };
        promociones.Add(promocion10Productos);

        return promociones;
    }
}