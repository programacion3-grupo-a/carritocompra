﻿namespace carritoCompra;

/// <summary>
/// Representa un conjunto de promociones especiales para el periodo navideño.
/// </summary>
public class PromocionNavidad
{

    /// <summary>
    /// Obtiene una lista de promociones válidas para la temporada navideña.
    /// </summary>
    /// <returns>Una lista de promociones para Navidad.</returns>
    public static List<Promocion> ObtenerPromocionesNavidad()
    {
        List<Promocion> promociones = new List<Promocion>();

        Promocion promocionNavidad = new Promocion
        {
            Descripcion = "Navidad - Descuento especial",
            PorcentajeDescuento = 0.25m,
            FechaInicio = new DateTime(2024, 12, 20),
            FechaFin = new DateTime(2024, 12, 26),
            Categoria = new Categoria { Nombre = "Todos" },
        };
        promociones.Add(promocionNavidad);

        return promociones;
    }
}
