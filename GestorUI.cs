﻿namespace carritoCompra;

    public class GestorUI
{
    public static void MostrarPromociones(List<Promocion> promociones)
    {
        foreach (var promocion in promociones)
        {
            string categoriaNombre = promocion.Categoria != null ? promocion.Categoria.Nombre : "Sin categoría";
            string fechaInicio = promocion.FechaInicio != DateTime.MinValue ? promocion.FechaInicio.ToString() : "N/A";
            string fechaFin = promocion.FechaFin != DateTime.MinValue ? promocion.FechaFin.ToString() : "N/A";
            Console.WriteLine($"Descripción: {promocion.Descripcion}, Categoría: {categoriaNombre}, Porcentaje de descuento: {promocion.PorcentajeDescuento:P}, Fecha de inicio: {fechaInicio}, Fecha de fin: {fechaFin}");
        }
    }

    public static void MostrarProductos(List<Producto> productos)
    {
        foreach (var producto in productos)
        {
            Console.WriteLine($"Nombre: {producto.Nombre}, Precio actual: {producto.Precio:C}");
        }
    }

    public static void MostrarDescuentosAplicados(List<Promocion> descuentos)
    {
        foreach (var descuento in descuentos)
        {
            Console.WriteLine($"Descuento: {descuento.Descripcion}, Porcentaje: {descuento.PorcentajeDescuento:P}");
        }
    }

    public static void MostrarResumenCarrito(CarritoDeCompras carrito)
    {
        Console.WriteLine($"Fecha de compra: {carrito.FechaCompra.ToShortDateString()}");
        Console.WriteLine("Productos comprados:");
        foreach (var producto in carrito.Productos)
        {
            Console.WriteLine($"- Nombre: {producto.Nombre}, Precio actual: {producto.Precio:C}");
        }

        Console.WriteLine("\nDescuentos aplicados:");
        if (carrito.DescuentosAplicados.Count == 0)
        {
            Console.WriteLine("Ningún descuento aplicado.");
        }
        else
        {
            foreach (var descuento in carrito.DescuentosAplicados)
            {
                Console.WriteLine($"- Descuento: {descuento.Descripcion}, Porcentaje: {descuento.PorcentajeDescuento:P}");
            }
        }

        Console.WriteLine($"\nTotal con descuentos aplicados: {carrito.CalcularTotal():C}");
    }
}