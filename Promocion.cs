﻿namespace carritoCompra;

/// <summary>
/// Representa una promoción aplicable a productos en un carrito de compras.
/// </summary>
public class Promocion
{
    public string Descripcion { get; set; }
    public Categoria Categoria { get; set; }
    public decimal PorcentajeDescuento { get; set; }
    public int CantidadMinimaProductos { get; set; }
    public DateTime FechaInicio { get; set; }
    public DateTime FechaFin { get; set; }
}