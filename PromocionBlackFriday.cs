﻿namespace carritoCompra;

/// <summary>
/// Representa un conjunto de promociones especiales para el evento Black Friday.
/// </summary>
public class PromocionBlackFriday
{

    /// <summary>
    /// Obtiene una lista de promociones válidas para el evento Black Friday.
    /// </summary>
    /// <returns>Una lista de promociones para Black Friday.</returns>
    public static List<Promocion> ObtenerPromocionesBlackFriday()
    {
        List<Promocion> promociones = new List<Promocion>();

        Promocion promocionElectronica = new Promocion
        {
            Descripcion = "Black Friday - Electrónica",
            Categoria = new Categoria { Nombre = "Electrónica" },
            PorcentajeDescuento = 0.30m,
            FechaInicio = new DateTime(2024, 11, 25),
            FechaFin = new DateTime(2024, 12, 1)
        };
        promociones.Add(promocionElectronica);

        Promocion promocionLibros = new Promocion
        {
            Descripcion = "Black Friday - Libros",
            Categoria = new Categoria { Nombre = "Libros" },
            PorcentajeDescuento = 0.40m,
            FechaInicio = new DateTime(2024, 11, 25),
            FechaFin = new DateTime(2024, 12, 1)
        };
        promociones.Add(promocionLibros);

        Promocion promocionRopa = new Promocion
        {
            Descripcion = "Black Friday - Ropa",
            Categoria = new Categoria { Nombre = "Ropa" },
            PorcentajeDescuento = 0.50m,
            FechaInicio = new DateTime(2024, 11, 25),
            FechaFin = new DateTime(2024, 12, 1)
        };
        promociones.Add(promocionRopa);

        return promociones;
    }
}