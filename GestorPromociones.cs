﻿namespace carritoCompra;
  public class GestorPromociones
{
    public static void AplicarDescuentos(CarritoDeCompras carrito, List<Promocion> promocionesNavidad, List<Promocion> promocionesBlackFriday, List<Promocion> promocionesCantidad)
    {
        foreach (var promocion in promocionesNavidad)
        {
            if (carrito.FechaCompra >= promocion.FechaInicio && carrito.FechaCompra <= promocion.FechaFin && carrito.Productos.Count >= promocion.CantidadMinimaProductos)
            {
                carrito.AplicarDescuento(promocion);
            }
        }

        foreach (var promocion in promocionesBlackFriday)
        {
            if (carrito.FechaCompra >= promocion.FechaInicio && carrito.FechaCompra <= promocion.FechaFin)
            {
                carrito.AplicarDescuento(promocion);
            }
        }

        var promocionesOrdenadas = promocionesCantidad.OrderByDescending(p => p.CantidadMinimaProductos);
        foreach (var promocion in promocionesOrdenadas)
        {
            if (carrito.Productos.Count >= promocion.CantidadMinimaProductos)
            {
                carrito.AplicarDescuento(promocion);
                break;
            }
        }
    }
}