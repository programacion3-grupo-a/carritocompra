﻿namespace carritoCompra;

/// <summary>
/// Clase que representa una categoría de producto.
/// </summary>
public class Categoria
{
    public string Nombre { get; set; }
}
