﻿namespace carritoCompra;

/// <summary>
/// Representa un carrito de compras que contiene una lista de productos.
/// </summary>
public class CarritoDeCompras
{
    public List<Producto> Productos { get; private set; }
    public decimal Total { get; private set; }
    public const string TodosCategorias = "Todos";
    public DateTime FechaCompra { get; set; }
    public List<Promocion> DescuentosAplicados { get; private set; }

    public CarritoDeCompras()
    {
        DescuentosAplicados = new List<Promocion>();
        Productos = new List<Producto>();
    }

    public void AgregarProducto(Producto producto)
    {
        Producto nuevoProducto = new Producto
        {
            Nombre = producto.Nombre,
            Categoria = producto.Categoria,
            Precio = producto.Precio
        };
        Productos.Add(nuevoProducto);
    }

    /// <summary>
    /// Calcula el total del precio de todos los productos en el carrito.
    /// </summary>
    /// <returns>El total del precio de los productos en el carrito.</returns>
    public decimal CalcularTotal()
    {
        Total = Productos.Sum(producto => producto.Precio);
        return Total;
    }

    /// <summary>
    /// Aplica un descuento al carrito y a todos los productos que cumplan con los criterios del descuento.
    /// </summary>
    /// <param name="descuento">El descuento a aplicar.</param>
    public void AplicarDescuento(Promocion descuento)
    {
        foreach (var producto in Productos)
        {
            if (producto.Categoria.Nombre == descuento.Categoria.Nombre)
            {
                producto.Precio = producto.ObtenerPrecioConDescuento(descuento.PorcentajeDescuento);
            }else if (descuento.Categoria.Nombre == TodosCategorias)
            {
                producto.Precio = producto.ObtenerPrecioConDescuento(descuento.PorcentajeDescuento);
            }
        }
        DescuentosAplicados.Add(descuento); 
    }
} 