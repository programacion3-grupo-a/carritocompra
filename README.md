# Carrito de Compras

Este proyecto implementa un sistema simple de carrito de compras en C#, donde se pueden agregar productos al carrito y aplicar diferentes promociones, como descuentos por cantidad, promociones de Black Friday y promociones navide�as.

## Estructura del Proyecto

El proyecto consta de los siguientes archivos y carpetas:

- **carritoCompra**: Carpeta que contiene las clases relacionadas con el carrito de compras y su funcionalidad.
  - `CarritoDeCompras.cs`: Clase principal que representa el carrito de compras y contiene m�todos para agregar productos, calcular el total y aplicar descuentos.
  - `Producto.cs`: Clase que define la estructura de un producto.
  - `Categoria.cs`: Clase que define la estructura de una categor�a de productos.
  - `Promocion.cs`: Clase que define la estructura de una promoci�n.
  - `PromocionBlackFriday.cs`: Clase que proporciona m�todos para obtener las promociones de Black Friday.
  - `PromocionCantidad.cs`: Clase que proporciona m�todos para obtener las promociones por cantidad.
  - `PromocionNavidad.cs`: Clase que proporciona m�todos para obtener las promociones de Navidad.
- **Program.cs**: Archivo que contiene el punto de entrada del programa y ejemplos de uso del carrito de compras y las promociones.
- **GestorPromociones.cs**: Clase que gestiona la aplicaci�n de descuentos en el carrito de compras.
- **GestorUI.cs**: Clase que proporciona m�todos para mostrar informaci�n en la interfaz de usuario.

## Uso del Programa

El archivo `Program.cs
