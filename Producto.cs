﻿namespace carritoCompra;

/// <summary>
/// Representa un producto que puede ser agregado a un carrito de compras.
/// </summary>
public class Producto
{
    public string Nombre { get; set; }
    public Categoria Categoria { get; set; }
    public decimal Precio { get; set; }

    /// <summary>
    /// Calcula el precio del producto con el descuento especificado.
    /// </summary>
    /// <param name="descuento">El porcentaje de descuento a aplicar.</param>
    /// <returns>El precio del producto con el descuento aplicado.</returns>
    public decimal ObtenerPrecioConDescuento(decimal descuento)
    {
        decimal descuentoProducto = Precio * descuento;
        return Precio - descuentoProducto;
    }
}
