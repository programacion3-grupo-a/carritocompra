﻿namespace carritoCompra;

/// <summary>
/// Clase principal que contiene el punto de entrada del programa.
/// </summary>
public class Program
{
    static void Main(string[] args)
    {
        List<Promocion> promocionesBlackFriday = PromocionBlackFriday.ObtenerPromocionesBlackFriday();
        List<Promocion> promocionesNavidad = PromocionNavidad.ObtenerPromocionesNavidad();
        List<Promocion> promocionesCantidad = PromocionCantidad.ObtenerPromocionesCantidad();

        Categoria electronica = new Categoria { Nombre = "Electrónica" };
        Categoria libros = new Categoria { Nombre = "Libros" };
        Categoria ropa = new Categoria { Nombre = "Ropa" };

        Producto laptop = new Producto { Nombre = "Laptop", Categoria = electronica, Precio = 1000};
        Producto smartphone = new Producto { Nombre = "Smartphone", Categoria = electronica, Precio = 500};
        Producto auriculares = new Producto { Nombre = "Auriculares", Categoria = electronica, Precio = 50 };
        Producto relojInteligente = new Producto { Nombre = "Reloj Inteligente", Categoria = electronica, Precio = 150 };
        Producto novelas = new Producto { Nombre = "Novelas", Categoria = libros, Precio = 15 };
        Producto libro = new Producto { Nombre = "Libro", Categoria = libros, Precio = 20 };
        Producto camisa = new Producto { Nombre = "Camisa", Categoria = ropa, Precio = 30 };
        Producto pantalon = new Producto { Nombre = "Pantalón", Categoria = ropa, Precio = 40 };
        Producto sueter = new Producto { Nombre = "Suéter", Categoria = ropa, Precio = 50 };
        Producto jeans = new Producto { Nombre = "Jeans", Categoria = ropa, Precio = 35 };

        CarritoDeCompras carritoNavidad = new CarritoDeCompras();
        carritoNavidad.FechaCompra = new DateTime(2024, 12, 24);
        carritoNavidad.AgregarProducto(laptop);
        carritoNavidad.AgregarProducto(smartphone);
        carritoNavidad.AgregarProducto(auriculares);
        carritoNavidad.AgregarProducto(relojInteligente);
        carritoNavidad.AgregarProducto(novelas);
        carritoNavidad.AgregarProducto(libro);

        CarritoDeCompras carritoBlackFriday = new CarritoDeCompras();
        carritoBlackFriday.FechaCompra = new DateTime(2024, 11, 27);
        carritoBlackFriday.AgregarProducto(laptop);
        carritoBlackFriday.AgregarProducto(camisa);
        carritoBlackFriday.AgregarProducto(pantalon);
        carritoBlackFriday.AgregarProducto(jeans);

        CarritoDeCompras carritoCantidad = new CarritoDeCompras();
        carritoCantidad.FechaCompra = new DateTime(2025, 10, 15);
        carritoCantidad.AgregarProducto(smartphone);
        carritoCantidad.AgregarProducto(relojInteligente);
        carritoCantidad.AgregarProducto(camisa);
        carritoCantidad.AgregarProducto(sueter);
        carritoCantidad.AgregarProducto(jeans);

        Console.WriteLine("Todas las promociones:");
        GestorPromociones.AplicarDescuentos(carritoNavidad, promocionesNavidad, promocionesBlackFriday, promocionesCantidad);
        GestorPromociones.AplicarDescuentos(carritoBlackFriday, promocionesNavidad, promocionesBlackFriday, promocionesCantidad);
        GestorPromociones.AplicarDescuentos(carritoCantidad, promocionesNavidad, promocionesBlackFriday, promocionesCantidad);

        GestorUI.MostrarPromociones(promocionesNavidad);
        GestorUI.MostrarPromociones(promocionesBlackFriday);
        GestorUI.MostrarPromociones(promocionesCantidad);

        Console.WriteLine("\n\nCarrito con promoción de Navidad:");
        GestorUI.MostrarResumenCarrito(carritoNavidad);

        Console.WriteLine("\nCarrito con promoción de Black Friday:");
        GestorUI.MostrarResumenCarrito(carritoBlackFriday);

        Console.WriteLine("\nCarrito con promoción por cantidad:");
        GestorUI.MostrarResumenCarrito(carritoCantidad);
    }
}